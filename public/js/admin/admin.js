
var nav_item = document.querySelectorAll('.nav-list ul li a');
[].forEach.call(nav_item, function(item) {
    item.addEventListener('click', function() {

        var clickeditem  = item.parentNode.childNodes[0];
        var active_class = document.querySelector('.nav-list ul li .active');

        //adding active class to the clicked anchor
        active_class.classList.remove('active');
        clickeditem.classList.add('active');
    });

});