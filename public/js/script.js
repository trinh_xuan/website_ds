/*

Style   : MobApp Script JS
Version : 1.0
Author  : Surjith S M
URI     : https://surjithctly.in/

Copyright © All rights Reserved 

*/

$(function() {
    "use strict";

    /*-----------------------------------
     * FIXED  MENU - HEADER
     *-----------------------------------*/
     function menuscroll() {
        var $navmenu = $('.nav-menu');
        if ($(window).scrollTop() > 50) {
            $navmenu.addClass('is-scrolling animated slideInDown');
        } else {
            $navmenu.removeClass("is-scrolling animated slideInDown");
        }
    }

    

    menuscroll();
    $(window).on('scroll', function() {
        menuscroll();
    });
    /*-----------------------------------
     * NAVBAR CLOSE ON CLICK
     *-----------------------------------*/

     $('.navbar-nav > li:not(.dropdown) > a').on('click', function() {
        $('.navbar-collapse').collapse('hide');
    });
    /* 
     * NAVBAR TOGGLE BG
     *-----------------*/
     var siteNav = $('#navbar');
     siteNav.on('show.bs.collapse', function(e) {
        $(this).parents('.nav-menu').addClass('menu-is-open');
    })
     siteNav.on('hide.bs.collapse', function(e) {
        $(this).parents('.nav-menu').removeClass('menu-is-open');
    })

    /*-----------------------------------
     * ONE PAGE SCROLLING
     *-----------------------------------*/
    // Select all links with hashes
    $('a[href*="#"]').not('[href="#"]').not('[href="#0"]').not('[data-toggle="tab"]').on('click', function(event) {
        // On-page links
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            // Figure out element to scroll to
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            // Does a scroll target exist?
            if (target.length) {
                // Only prevent default if animation is actually gonna happen
                event.preventDefault();
                $('html, body').animate({
                    scrollTop: target.offset().top
                }, 1000, function() {
                    // Callback after animation
                    // Must change focus!
                    var $target = $(target);
                    $target.focus();
                    if ($target.is(":focus")) { // Checking if the target was focused
                        return false;
                    } else {
                        $target.attr('tabindex', '-1'); // Adding tabindex for elements not focusable
                        $target.focus(); // Set focus again
                    };
                });
            }
        }
    });
    /*-----------------------------------
     * OWL CAROUSEL
     *-----------------------------------*/
     var $testimonialsDiv = $('.testimonials');
     if ($testimonialsDiv.length && $.fn.owlCarousel) {
        $testimonialsDiv.owlCarousel({
            items: 1,
            nav: true,
            dots: false,
            navText: ['<span class="fa fa-arrow-left"></span>', '<span class="fa fa-arrow-right"></span>']
        });
    }

    var $galleryDiv = $('.img-gallery');
    if ($galleryDiv.length && $.fn.owlCarousel) {
        $galleryDiv.owlCarousel({
            nav: false,
            center: true,
            loop: true,
            // autoplay: true,
            dots: true,
            navText: ['<span class="fa fa-arrow-left"></span>', '<span class="fa fa-arrow-right"></span>'],
            responsive: {
                1199: {
                    items: 3,
                },
                991: {
                    items: 2,
                },
                768: {
                    items: 2,
                },
                479: {
                    items: 1,
                },
                0: {
                    items: 1,
                }
            }
        });
    }



    $('.slide-banner').owlCarousel({
        loop: true,
        margin: 0,
        responsiveClass: true,
        nav: false,
        dots: true,
        smartSpeed: 1500,
        autoplay: true,
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
        autoplayTimeout: 2500,
        navClass: ["slide-prev", "slide-next"],
        navText: ["<i class='arrow_carrot-left'></i>", "<i class='arrow_carrot-right'></i>"],
        responsive: {
            1199: {
                items: 1,
            },
            991: {
                items: 1,
            },
            768: {
                items: 1,
            },
            479: {
                items: 1,
            },
            0: {
                items: 1,
            }
        }
    });


    $('.slide-logo-ft').owlCarousel({
        loop: true,
        margin: 0,
        responsiveClass: true,
        nav: false,
        dots: true,
        smartSpeed: 1500,
        autoplay: true,
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
        autoplayTimeout: 2500,
        navClass: ["slide-prev", "slide-next"],
        navText: ["<i class='  arrow_carrot-left'></i>", "<i class='  arrow_carrot-right'></i>"],
        responsive: {
            1199: {
                items: 5,
            },
            991: {
                items: 4,
            },
            768: {
                items: 3,
            },
            479: {
                items: 2,
            },
            0: {
                items: 2,
            }
        }
    });

// Search-menu

$('.search-toggle').addClass('closed');
$('.search-toggle .search-icon').click(function(e) {
  if ($('.search-toggle').hasClass('closed')) {
    $('.search-toggle').removeClass('closed').addClass('opened');
    $('.search-toggle, .search-container').addClass('opened');
    $('#search-terms').focus();
} else {
    $('.search-toggle').removeClass('opened').addClass('closed');
    $('.search-toggle, .search-container').removeClass('opened');
}
});





////slide-banner

$('.circle:first-child').addClass('active')

$('.circle').on('click', function(){
  $(this).addClass('active')
  $(this).siblings().removeClass('active')
})

$('#proxy-nav').on('click', function(){
  $('#slider').animate({
    'left': '5%'
})
  $('#proxy-main .content').fadeIn('fast')
  $('#arty-main .content').fadeOut('fast')
  $('#vassili-main .content').fadeOut('fast')
  $('#slider .line').animate({
    'left': '60%'
}, 500)
})

$('#arty-nav').on('click', function(){
  $('#slider').animate({
    'left': '-30%'
})
  $('#arty-main .content').fadeIn('fast')
  $('#proxy-main .content').fadeOut('fast')
  $('#vassili-main .content').fadeOut('fast')
  $('#slider .line').animate({
    'left': '25%'
}, 500)
})

$('#vassili-nav').on('click', function(){
  $('#slider').animate({
    'left': '-195%'
})
  $('#vassili-main .content').fadeIn('fast')
  $('#proxy-main .content').fadeOut('fast')
  $('#arty-main .content').fadeOut('fast')
  $('#slider .line').animate({
    'left': '35%'
}, 500)
})

const l1 = $('#p')
const l2 = $('#a')
const l3 = $('#v')

const layer = $('#main')

layer.mousemove(function(e){
  const valueX = (e.pageX * -1 / 40)
  const valueY = (e.pageY * -1 / 40)
  
  l1.css("background-position", (valueX-180) +"px     "+(valueY-50)+"px");
  l2.css("background-position", (valueX+1000) +"px     "+(valueY-50)+"px");
  l3.css("background-position", (valueX-150) +"px     "+(valueY-50)+"px");
})

$(document).keydown(function(e) {

  switch(e.which) {
    case 37:  // left
    if ($('.active').prev().length > 0){
        $('.active').prev().trigger('click') // right
    }
    break;

    case 39:
    if ($('.active').next().length > 0){
        $('.active').next().trigger('click') // right
    }
    break;

    default: return; // exit this handler for other keys
}
  e.preventDefault(); // prevent the default action (scroll / move caret)
});





// Scroll to TOP

$(window).scroll(function() {
    if ($(this).scrollTop() > 100) {
        $('.scroll_top').fadeIn();
    } else {
        $('.scroll_top').fadeOut();
    }
});
$('.scroll_top').on('click', function() {
    $("html, body").animate({
        scrollTop: 0
    }, 1000);
    return false;
});
$(window).scroll(function() {
    var scroll = $(window).scrollTop();
    if (scroll > 500) {
        $(".intro").hide();
        $(".scroll_down").hide();
    }
});


//fade-In/Out title-banner

$(window).scroll(function() {
    if ($(this).scrollTop() > 100) {
        $('#ct1').fadeOut();
    } else {
        $('#ct1').fadeIn();
    }
});
$(window).scroll(function() {
    if ($(this).scrollTop() > 100) {
        $('#ct2').fadeOut();
    } else {
        $('#ct2').fadeIn();
    }
});
$(window).scroll(function() {
    if ($(this).scrollTop() > 100) {
        $('#ct3').fadeOut();
    } else {
        $('#ct3').fadeIn();
    }
});














}); /* End Fn */