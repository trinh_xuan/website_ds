<?php

Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {
    Route::get('/', 'Admin\HomePageController@index')->name('admin');
});

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
