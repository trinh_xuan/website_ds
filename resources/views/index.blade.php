<!doctype html>
<html lang="{{ app()->getLocale() }}">
<!--

Page    : index / MobApp
Version : 1.0
Author  : Colorlib
URI     : https://colorlib.com

-->

<head>
    <title>Công ty Phần mềm Công nghệ và Giải pháp DS - DS Company</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="MSB-bank - Đăng ký nhận ngay 10k">
    <meta name="keywords" content="Đăng ký nhận thẻ 10k, msb, msb bank">
    <link rel="shortcut icon" type="text/css" href="images/favicon.png">

    <!-- Font -->
    <link href="{{asset('fonts/elegantIcon/elegantIcon.css')}}" type="text/css" rel="stylesheet">
    <link href="{{asset('fonts/font-awesome/css/font-awesome.min.css')}}" type="text/css" rel="stylesheet">
    <link rel="dns-prefetch" href="//fonts.googleapis.com">
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href={{asset('css/bootstrap.min.css')}}>
    <!-- Themify Icons -->
    <link rel="stylesheet" href="{{asset('css/themify-icons.css')}}">
    <!-- Owl carousel -->
    <link rel="stylesheet" href="{{asset('css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/slick.css')}}">
    <link rel="stylesheet" href="{{asset('css/animate.css')}}">
    <link rel="stylesheet" href="{{asset('css/hover-min.css')}}">
    <!-- Main css -->
    <link href="{{asset('css/style.css')}}" rel="stylesheet">
</head>

<body data-spy="scroll" data-target="#navbar" data-offset="30">

<!-- Nav Menu -->

<div class="nav-menu fixed-top">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <nav class="navbar navbar-dark navbar-expand-lg">
                    <a class="navbar-brand" href="{{route('home')}}"><img src="images/logo.png" class="img-fluid" alt="logo"></a> <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button>
                    <div class="collapse navbar-collapse " id="navbar">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item"> <a class="nav-link active" href="{{route('home')}}">TRANG CHỦ <span class="sr-only">(current)</span></a> </li>
                            <li class="nav-item"> <a class="nav-link" href="#features">GIỚI THIỆU</a> </li>
                            <li class="nav-item"> <a class="nav-link" href="#video-msb">DỊCH VỤ</a> </li>
                            <li class="nav-item"> <a class="nav-link" href="#tienich-msb">SẢN PHẨM</a> </li>
                            <li class="nav-item"> <a class="nav-link" href="#gallery">TIN TỨC</a> </li>
                            <li class="nav-item"> <a class="nav-link" href="#gallery-2">TUYỂN DỤNG</a> </li>
                            <li class="nav-item"> <a class="nav-link" href="#contact">LIÊN HỆ</a> </li>
                        </ul>

                        <div class="home search-form">
                            <div class="search-toggle">
                                <button class="search-icon icon-search hvr-icon-push"><i class=" icon_search"></i></button>
                                <button class="search-icon icon-close"><i class=" icon_close_alt2"></i></button>
                            </div>
                            <div class="search-container">
                                <form>
                                    <input type="text" name="q" id="search-terms" placeholder="Search..." />
                                    <button type="submit" name="submit" value="Go" class="search-icon"><i class="fa fa-fw fa-search"></i></button>
                                </form>
                            </div>
                        </div>

                        <div class="lang">
                            <div class="dropdown">
                                <button onclick="myFunction()" class="dropbtn"><img src="images/lang-vi.png" alt="">Tiếng Việt</button>
                                <div id="myDropdown" class="dropdown-content">
                                    <a href="" class="bt"><img src="images/lang-en.png" alt="">English</a>
                                    <a href=""><img src="images/lang-vi.png" alt="">Tiếng  Việt</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</div>

<div class="menu-mobile">
    <div class="container">

    </div>
</div>

<div id="main">
    <div id="slider">
        <!-- <div class="line"></div> -->
        <div id="proxy-main" class="col-md-12">
            <div class="letter " id="p">D</div>
            <div class="content  proxy wow zoomIn animated" id="proxy-content">
                <div id="ct1">
                    <div class="title-banner-top">Ds.CO</div>
                    <div class="info" >Công ty TNHH Công nghệ và Giải pháp DS (Tên viết tắt DSCo) - Tên giao dịch quốc tế: Technology and Solution DS Company (DSCo).</div>
                </div>
            </div>
        </div>

        <div id="arty-main" class="col-md-12">
            <div class="letter" id="a">S</div>
            <div class="content arty wow zoomIn animated" id="arty-content">
                <div id="ct2">
                    <div class="title-banner-top">Solution</div>
                    <div class="info">(DSCo) Với đội ngũ lãnh đạo và nhân viên đầy sức trẻ, sáng tạo và tận tụy, cùng với sự quan tâm chiến lược của nhiều cá nhân và tổ chức trong lĩnh vực Công nghệ thông tin.</div>
                </div>
            </div>
        </div>

        <div id="vassili-main" class="col-md-12">
            <div class="letter" id="v">C</div>
            <div class="content vassili wow zoomIn animated" id="bushwacker-content">
                <div id="ct3">
                    <div class="title-banner-top">Co<!-- <span>o</span> -->mpany</div>
                    <div class="info">(DSCo) Một công ty trẻ đầy triển vọng chúng tôi quyết tâm mang sứ mệnh đưa ngành công nghiệp phần mềm cũng như công nghệ tự động hóa của Việt Nam sánh ngang tầm với thế giới.</div>
                </div>
            </div>
        </div>
    </div>

    <div id="navigation">
        <a href="#" class="circle" id="proxy-nav"></a>
        <a href="#" class="circle" id="arty-nav"></a>
        <a href="#" class="circle" id="vassili-nav"></a>
    </div>
</div>



<div class="section light-bg" id="features">
    <div class="section light-bg">
        <div class="container">
            <div class="section-title wow zoomIn animated ">
                <h2>Về chúng tôi</h2>
                <img src="images/bt.png" alt="">
            </div>
            <div class="row">
                <div class="col-md-6 wow fadeInRight top__element" data-wow-delay="0.3s" data-wow-duration="1.5s">
                    <div class="img-ab ">
                        <a href=""><img src="images/img9.jpg" alt="" style="width: 100%;"></a>
                        <a class="date" href="">06 <span>TH.07</span></a>
                        <div class="paragrab">
                            <h4><a href="">Công ty TNHH Công nghệ và Giải pháp DS</a></h4>
                            <div class="news-event-ab">
                                <a href=""><span class="fa fa-tag"></span>Tin tức sự kiện</a>
                                <a href=""><span class="fa fa-eye"></span>999 Lượt xem</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="about wow fadeInUp top__element" data-wow-delay="0.3s" data-wow-duration="1.5s">
                        <h4 class="title">tầm nhìn và chiến lược<i class="dot"></i></h4>
                        <p>Chúng tôi với đội ngũ trẻ trung năng động, đầy nhiệt huyết và cùng niềm đam mê. Đặc biệt là chúng tôi luôn hướng tới một đích là đưa <span> DS Co., Ltd vươn xa trên thị trường quốc tế </span>, để có thể đem đến những giải pháp công nghệ thông minh nhất, mới nhất, thực tế nhất cho  những doanh nghiệp, những cá nhân trên toàn cầu.
                        </p>
                        <p>Với đội ngũ lãnh đạo và nhân viên đầy sức trẻ, sáng tạo và tận tụy, cùng với sự quan tâm chiến lược của nhiều cá nhân và tổ chức trong lĩnh vực Công nghệ thông tin, DSCo tham gia vào những lĩnh vực mũi nhọn như sau:</p>
                        <a class="a1 v2 " href="about-us.html">xem thêm</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="section" id="video-msb">
    <!--  <canvas id="canvas" width="100%" height="100%"></canvas> -->

    <div class="section">

        <div class="container">
            <div class="section-title wow fadeInUp top__element" data-wow-delay="0.3s" data-wow-duration="1.5s">
                <h2>dịch vụ</h2>
                <img src="images/bt.png" alt="">
            </div>
            <div class="row">
                <div class="col-md-8">
                    <div class="list-service">
                        <a href=""><span class="icon_desktop"></span></a>
                        <div class="items wow fadeInUp top__element" data-wow-delay="0.3s" data-wow-duration="1.5s">
                            <h4>Phần mềm cơ quan, doanh nghiệp</h4>
                            <p>Trong cuộc sống tất bật hiện nay, mọi việc đều được đơn giản và tối ưu hóa thời gian thông qua việc ứng dụng công nghệ thông tin và xử lý các công việc hàng ngày.
                            </p>
                            <p>DS Co., Ltd chuyên cung cấp các Công nghệ và Giải pháp phần mềm cho các doanh nghiệp trong và ngoài nước.
                            </p>
                        </div>
                    </div>
                    <div class="list-service">
                        <a href=""><span class=" icon_mobile"></span></a>
                        <div class="items wow fadeInUp top__element" data-wow-delay="0.3s" data-wow-duration="1.5s">
                            <h4>Trò chơi và Ứng dụng mobile</h4>
                            <p>DS Co., Ltd được thành lập với mong muốn xây dựng một môi trường phát triển năng động, trẻ trung, tạo ra sự khác biệt với các tổ chức khác về chất lượng và quy mô mang tầm quốc tế.
                                Với sức trẻ và sự sáng tạo nhóm mong muốn phát hành những ứng dụng mang tầm quốc tế.</p>
                            <p class="contents-ser">- Chứng tỏ năng lực của bản thân trong việc phát triển, phát hành ứng dụng di động trong nước và cả quốc tế. DS Co sẽ sớm gia nhập vào hàng ngũ những nhà phát triển ứng dụng hàng đầu tại Việt Nam.</p>
                            <p class="contents-ser">- Làm giàu cho bản thân và xã hội. Thị trường ứng dụng di động là một mỏ vàng cho những ai biết cách đầu tư và khai thác hiệu quả.</p>
                            <p class="contents-ser">- Khẳng định con đường thành công cho giới trẻ, dám nghĩ, dám làm.</p>
                        </div>
                    </div>
                    <div class="faq-page wow fadeInUp top__element" data-wow-delay="0.3s" data-wow-duration="1.5s">
                        <div class="title-faq">
                            For more question kindly check our FAQ page
                        </div>
                        <div class="employer">
                            <a href="">FAQ for Employer</a>
                            <a class="app" href=""> FAQ for Applicants</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="img-service wow fadeInRight top__element" data-wow-delay="0.3s" data-wow-duration="1.5s">
                        <a href=""><img src="images/img6.jpg" alt="" style="width: 100%;"></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="section light-bg" id="tienich-msb">
    <canvas id="canvas" width="100%" height="100%"></canvas>
    <div class="container">
        <div class="section-title wow fadeInUp top__element" data-wow-delay="0.3s" data-wow-duration="1.5s">
            <h3>sản phẩm nổi bật</h3>
            <img src="images/bt.png" alt="">
        </div>

        <ul class="nav nav-tabs nav-justified wow zoomIn animated " role="tablist" >
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#communication">HỆ THỐNG</a>
            </li>
            <li class="nav-item">
                <a class="nav-link " data-toggle="tab" href="#schedule">PHẦN MỀM</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#messages">DỊCH VỤ</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#livechat">QUẢN LÝ</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#apps">ỨNG DỤNG</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#android">ANDROID</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#ios">IOS</a>
            </li>
        </ul>
        <div class="tab-content wow zoomIn animated ">
            <div class="tab-pane fade show active" id="communication">
                <div class="d-flex flex-column flex-lg-row">
                    <img src="images/msb-the-tra-truoc.jpg" alt="graphic" class="img-fluid rounded align-self-start mr-lg-5 mb-5 mb-lg-0">
                    <div>
                        <h2><a href="">Hệ thống phần mềm tự động hóa doanh nghiệp</a></h2>
                        <p class="lead" style="text-align:justify">Giúp doanh nghiệp xây dựng chiến lược kinh doanh từ khâu phân tích dung lượng thị trường, khả năng chiếm thị phần, giá bán cho các dự án, các sản phẩm.</p>
                        <p style="text-align:justify">Giúp doanh nghiệp xây dựng chiến lược kinh doanh từ khâu phân tích dung lượng thị trường, khả năng chiếm thị phần, giá bán cho các dự án, các sản phẩm.
                        </p>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="schedule">
                <div class="d-flex flex-column flex-lg-row">
                    <div>
                        <h2><a href="">Phần mềm quản lý nhân sự, chấm công, tính lương (HRM-DS)</a></h2>
                        <p class="lead" style="text-align:justify; margin-right:10px;">Sau đây là các lý do khiến cho phần mềm nhân sự – tiền lương là một trong những phần mềm có tỉ lệ sử dụng cao nhất trong doanh nghiệp:</p>
                        <p style="text-align:justify; margin-right:15px;"> Không cần mất thời gian tới Phòng giao dịch hay Chi nhánh của Ngân hàng, chỉ với một chiếc điện thoại thông minh, Khách hàng có thể tải ngay App MSB mBank và đăng ký dịch vụ ngân hàng số M-WOW. Với thiết kế đơn giản, thân thiện với người dùng, M-WOW mang đến cho Khách hàng đầy đủ tiện ích, tính năng các tài chính, thanh toán, mua sắm, giải trí chỉ với những thao tác hết sức dễ dàng, đơn giản, thuận tiện, nhanh chóng.
                        </p>
                    </div>
                    <img src="images/msb-dang-ky.jpg" alt="graphic" class="img-fluid rounded align-self-start mr-lg-5 mb-5 mb-lg-0">
                </div>
            </div>
            <div class="tab-pane fade" id="messages">
                <div class="d-flex flex-column flex-lg-row">
                    <img src="images/msb-bank-home-2.png" alt="graphic" class="img-fluid rounded align-self-start mr-lg-5 mb-5 mb-lg-0">
                    <div>
                        <h2><a href="">Dịch vụ công trực tuyến</a></h2>
                        <p class="lead">Cổng thanh toán online tiện ích mọi lúc mọi nơi.</p>
                        <p style="text-align:justify">Cơ sở dữ liệu về Dịch vụ Công là hệ thống thông tin về thủ tục hành chính và các văn bản pháp luật quy định về thủ tục hành chính liên quan đến cá nhân và tổ chức được thiết lập trên cơ sở các Quyết định công bố thủ tục hành chính hoặc thủ tục giải quyết công việc của bộ. Mục tiêu chính của cơ sở dữ liệu nhằm cung cấp một địa điểm duy nhất để người sử dụng có thể tìm kiếm các thủ tục hành chính quan tâm. Cơ sở dữ liệu sẽ tăng cường khả năng tiếp cận thông tin về các quy định, tăng tính minh bạch của hệ thống thể chế và thiết lập một cơ sở lịch sử về hệ thống thủ tục hành chính.
                        </p>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="livechat">
                <div class="d-flex flex-column flex-lg-row">
                    <div>
                        <h2><a href="">Quản lý công văn giấy tờ, điều hành công việc</a></h2>
                        <p class="lead">Tiêu xài thả ga – Không lo về giá</p>
                        <p style="text-align:justify;  margin-right:15px;">Không chỉ cho Khách hàng những trải nghiệm số độc đáo, ấn tượng, M-WOW còn mang tới cho hàng ngàn sản phẩm, dịch vụ với mức ưu đãi giảm giá lên tới hơn 90%. Khách hàng có thể thỏa thích lựa chọn mọi sản phẩm dịch vụ từ làm đẹp, spa, ăn uống cho tới xem phim, vui chơi, giải trí,… ngay trên App mà với chất lượng và mức giá “không thể tốt hơn”.
                        </p>
                    </div>
                    <img src="images/kv-msb.jpg" alt="graphic" class="img-fluid rounded align-self-start mr-lg-5 mb-5 mb-lg-0">
                </div>
            </div>
            <div class="tab-pane fade" id="apps">
                <div class="d-flex flex-column flex-lg-row">
                    <img src="images/kv-msb.jpg" alt="graphic" class="img-fluid rounded align-self-start mr-lg-5 mb-5 mb-lg-0">
                    <div>
                        <h2><a href="">Quản lý công văn giấy tờ, điều hành công việc</a></h2>
                        <p class="lead">Tiêu xài thả ga – Không lo về giá</p>
                        <p style="text-align:justify;  margin-right:15px;">Không chỉ cho Khách hàng những trải nghiệm số độc đáo, ấn tượng, M-WOW còn mang tới cho hàng ngàn sản phẩm, dịch vụ với mức ưu đãi giảm giá lên tới hơn 90%. Khách hàng có thể thỏa thích lựa chọn mọi sản phẩm dịch vụ từ làm đẹp, spa, ăn uống cho tới xem phim, vui chơi, giải trí,… ngay trên App mà với chất lượng và mức giá “không thể tốt hơn”.
                        </p>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="android">
                <div class="d-flex flex-column flex-lg-row">
                    <div>
                        <h2><a href="">Ứng dụng lịch vạn niên cho điện thoại Android</a></h2>
                        <p class="lead">Tiêu xài thả ga – Không lo về giá</p>
                        <p style="text-align:justify;  margin-right:15px;">Không chỉ cho Khách hàng những trải nghiệm số độc đáo, ấn tượng, M-WOW còn mang tới cho hàng ngàn sản phẩm, dịch vụ với mức ưu đãi giảm giá lên tới hơn 90%. Khách hàng có thể thỏa thích lựa chọn mọi sản phẩm dịch vụ từ làm đẹp, spa, ăn uống cho tới xem phim, vui chơi, giải trí,… ngay trên App mà với chất lượng và mức giá “không thể tốt hơn”.
                        </p>
                    </div>
                    <img src="images/kv-msb.jpg" alt="graphic" class="img-fluid rounded align-self-start mr-lg-5 mb-5 mb-lg-0">
                </div>
            </div>
            <div class="tab-pane fade" id="ios">
                <div class="d-flex flex-column flex-lg-row">
                    <img src="images/kv-msb.jpg" alt="graphic" class="img-fluid rounded align-self-start mr-lg-5 mb-5 mb-lg-0">
                    <div>
                        <h2><a href="">Ứng dụng lịch vạn niên cho điện thoại ios</a></h2>
                        <p class="lead">Tiêu xài thả ga – Không lo về giá</p>
                        <p style="text-align:justify;  margin-right:15px;">Không chỉ cho Khách hàng những trải nghiệm số độc đáo, ấn tượng, M-WOW còn mang tới cho hàng ngàn sản phẩm, dịch vụ với mức ưu đãi giảm giá lên tới hơn 90%. Khách hàng có thể thỏa thích lựa chọn mọi sản phẩm dịch vụ từ làm đẹp, spa, ăn uống cho tới xem phim, vui chơi, giải trí,… ngay trên App mà với chất lượng và mức giá “không thể tốt hơn”.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- // end .section -->

<div class="section light-bg news" id="gallery">
    <div class="container">
        <div class="section-title wow fadeInUp top__element" data-wow-delay="0.3s" data-wow-duration="1.5s">
            <h3>Tin Tức</h3>
            <img src="images/bt.png" alt="">
        </div>
        <div class="slider-news wow fadeInUp top__element" data-wow-delay="0.3s" data-wow-duration="1.5s">
            <div class="img-gallery owl-carousel owl-theme">
                <div class="item ">
                    <a href=""><img src="images/img3.jpg" alt=""></a>
                    <div class="contents-home">
                        <h4><a href="">5 công ty hàng đầu thế giới kiếm tiền như nào ?</a></h4>
                        <div class="content-1">
                            <a href=""><span class="fa fa-pencil-square-o"></span>John lee</a>
                            <a href=""><span class="fa fa-calendar"></span>2 Days ago</a>
                            <a href=""><span class="fa fa-comments"></span>12 comments</a>
                        </div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim niam...</p>
                        <div class="date">06 <span> TH.07 </span></div>
                    </div>
                    <div class="link"><a href=""><span class="fa fa-eye"></span>xem chi tiết</a></div>
                </div>
                <div class="item">
                    <a href=""><img src="images/img2.jpg" alt=""></a>
                    <div class="contents-home">
                        <h4><a href="">Nguồn nhân lực chính là tài sản quý giá.</a></h4>
                        <div class="content-1">
                            <a href=""><span class="fa fa-pencil-square-o"></span>John lee</a>
                            <a href=""><span class="fa fa-calendar"></span>2 Days ago</a>
                            <a href=""><span class="fa fa-comments"></span>12 comments</a>
                        </div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim niam...</p>
                        <div class="date">06 <span> TH.07 </span></div>
                    </div>
                    <div class="link"><a href=""><span class="fa fa-eye"></span>xem chi tiết</a></div>
                </div>
                <div class="item">
                    <a href=""><img src="images/img4.jpg" alt=""></a>
                    <div class="contents-home">
                        <h4><a href="">Tất cả đều hoạt động trên web.</a></h4>
                        <div class="content-1">
                            <a href=""><span class="fa fa-pencil-square-o"></span>John lee</a>
                            <a href=""><span class="fa fa-calendar"></span>2 Days ago</a>
                            <a href=""><span class="fa fa-comments"></span>12 comments</a>
                        </div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim niam...</p>
                        <div class="date">06 <span> TH.07 </span></div>
                    </div>
                    <div class="link"><a href=""><span class="fa fa-eye"></span>xem chi tiết</a></div>
                </div>
                <div class="item">
                    <a href=""><img src="images/img8.jpg" alt=""></a>
                    <div class="contents-home">
                        <h4><a href="">Những cử nhân, thạc sĩ tự rao mình tìm việc ?</a></h4>
                        <div class="content-1">
                            <a href=""><span class="fa fa-pencil-square-o"></span>John lee</a>
                            <a href=""><span class="fa fa-calendar"></span>2 Days ago</a>
                            <a href=""><span class="fa fa-comments"></span>12 comments</a>
                        </div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim niam...</p>
                        <div class="date">06 <span> TH.07 </span></div>
                    </div>
                    <div class="link"><a href=""><span class="fa fa-eye"></span>xem chi tiết</a></div>
                </div>
            </div>
            <div class="readmore">
                <a href="news.html" class="hvr-radial-out">Xem tất cả</a>
            </div>
        </div>
    </div>
</div>
<!-- // end .section -->


<div class="outer">
    <div class="overlay-home"></div>
    <div class="inner">
        <div class="section" id="gallery-2">
            <div class="container">
                <div class="section-title wow fadeInUp top__element" data-wow-delay="0.3s" data-wow-duration="1.5s">
                    <h3 class="title-home">Tuyển dụng</h3>
                    <img src="images/bt.png" alt="">
                </div>
                <div class="row">
                    <div class="inner-ct col-md-6 wow fadeInUp top__element" data-wow-delay="0.3s" data-wow-duration="1.5s">
                        <h4 class="title-left-acc">Tuyển dụng Nhân sự vị trí Hành chính - Văn phòng </h4>
                        <p>
                            Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.
                        </p>
                        <div class="readmore read-v2">
                            <a href="recruitment.html" class="hvr-radial-out">Xem thêm</a>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="accordion wow fadeInUp top__element" data-wow-delay="0.3s" data-wow-duration="1.5s">
                            <form class="acc">
                                <div>
                                    <input type="radio" name="size" id="small" value="small" checked="checked" />
                                    <label for="small"><a class="date" href="">06 <span>TH.07</span></a>Chiêu mộ nhân tài Nodejs lương khủng</label>
                                    <article>
                                        <p>You see? It's curious. Ted did figure it out - time travel. And when we get back, we gonna tell everyone. How it's possible, how it's done, what the dangers are. But then why fifty years in the future when the spacecraft encounters a black hole does the computer call it an 'unknown entry event'? Why don't they know? If they don't know, that means we never told anyone. And if we never told anyone it means we never made it back. Hence we die down here. Just as a matter of deductive logic.</p>
                                    </article>
                                </div>
                                <div>
                                    <input type="radio" name="size" id="medium" value="medium" />
                                    <label for="medium"><a class="date" href="">06 <span>TH.07</span></a>Tuyển dụng Nhân sự Phòng Marketing</label>
                                    <article>
                                        <p>You see? It's curious. Ted did figure it out - time travel. And when we get back, we gonna tell everyone. How it's possible, how it's done, what the dangers are. But then why fifty years in the future when the spacecraft encounters a black hole does the computer call it an 'unknown entry event'? Why don't they know? If they don't know, that means we never told anyone. And if we never told anyone it means we never made it back. Hence we die down here. Just as a matter of deductive logic.</p>
                                    </article>
                                </div>
                                <div>
                                    <input type="radio" name="size" id="large" value="large" />
                                    <label for="large"><a class="date" href="">06 <span>TH.07</span></a>Tuyển dụng vị trí Lập trình viên Java lương cao</label>
                                    <article>
                                        <p>You see? It's curious. Ted did figure it out - time travel. And when we get back, we gonna tell everyone. How it's possible, how it's done, what the dangers are. But then why fifty years in the future when the spacecraft encounters a black hole does the computer call it an 'unknown entry event'? Why don't they know? If they don't know, that means we never told anyone. And if we never told anyone it means we never made it back. Hence we die down here. Just as a matter of deductive logic.</p>
                                    </article>
                                </div>
                            </form>
                        </div> <!-- / accordion -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- // end .section -->

<div class="section bg-gradient" id="contact">
    <div class="container">
        <div class="section-title wow fadeInUp top__element" data-wow-delay="0.3s" data-wow-duration="1.5s">
            <small></small>
            <h3>liên hệ</h3>
            <img src="images/bt.png" alt="">
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="ct-left wow fadeInUp top__element" data-wow-delay="0.3s" data-wow-duration="1.5s">
                    <div class="address-ct">
                        <a href=""><img src="images/logo-ft.png" alt=""></a>
                        <h4><span class="fa fa-map-marker"></span>Số 30D, Kim Mã Thượng, Ba Đình, Hà Nội</h4>
                    </div>
                    <div class="phone">
                        <a href=""><span class="fa fa-phone"></span>+84 (4) 932 219 268</a>
                        <a href=""><span class="fa fa-fax"></span>+84 (4) 37959567</a>
                    </div>
                    <div class="email">
                        <a href=""><span class="fa fa-envelope"></span>info@ds.net.vn</a>
                    </div>
                    <div class="time">
                        <a href=""><span class="fa fa-clock-o"></span>Giờ làm việc: Thứ 2-6 / 8h30 sáng - 17h30 chiều.</a>
                        <p>- Văn phòng 1: Số 85 Nguyễn Phong Sắc, Cầu Giấy, Hà Nội</p>
                        <p>- Văn phòng 2: Số 102, tổ 8, Phường Minh Khai, Tp Hà Giang</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="single_contant_left wow fadeInUp top__element" data-wow-delay="0.3s" data-wow-duration="1.5s">
                    <form action="#" id="formid">
                        <div class="form-group">
                            <input type="text" class="form-control" name="name" placeholder="Họ và tên*" required="">
                            <input type="email" class="form-control" name="email" placeholder="Email*" required="">
                            <input type="text" class="form-control" name="subject" placeholder="Tiêu đề*" required="">
                            <textarea class="form-control" name="message" rows="7" placeholder="Nội dung*"></textarea>
                            <input type="submit" value="GỬI NGAY" class="btn btn-lg ">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- // end .section -->

<div class="slide-logos">
    <div class="container">
        <div class="section-title wow fadeInUp top__element" data-wow-delay="0.3s" data-wow-duration="1.5s">
            <small></small>
            <h3>khách hàng</h3>
            <img src="images/bt.png" alt="">
        </div>
        <div class="slide-logo-ft owl-carousel">
            <div class="item">
                <a href=""><img class="fillter" src="images/logo-1.png"></a>
            </div>
            <div class="item">
                <a href=""><img class="fillter" src="images/logo-3.png"></a>
            </div>
            <div class="item">
                <a href=""><img class="fillter-2" src="images/logo-4.jpg"></a>
            </div>
            <div class="item">
                <a href=""><img class="fillter-2" src="images/logo-5.png"></a>
            </div>
            <div class="item">
                <a href=""><img class="fillter" src="images/logo-6.png"></a>
            </div>
            <div class="item">
                <a href=""><img class="fillter-2" src="images/logo-7.png"></a>
            </div>
        </div>
    </div>
</div>

<div class="light-bg py-5 wow zoomIn animated" id="contact-ft">
    <div class="container">
        <div class="ft">
            <div class="ft-end">
                <ul class="flex-icons">
                    <li><a class="right" href=""><span class="fa fa-facebook"></span></a></li>
                    <li><a class="right" href=""><span class="fa fa-twitter"></span></a></li>
                    <li><a class="right" href=""><span class="fa fa-linkedin"></span></a></li>
                    <li><a href=""><span class="fa fa-google-plus"></span></a></li>
                </ul>
            </div>
            <div class="text-center">
                <!-- Copyright removal is not prohibited! -->
                <p class="mb-2"><small>©COPYRIGHT2019/DS-Co <a href="http://ds.net.vn">ds.net.vn</a></small></p>
            </div>
        </div>
    </div>
</div>
<!-- // end .section -->

<!-- scroll-top -->
<a href="#" class="zoa-btn scroll_top"><img src="images/left_32.png"></a>
<!-- jQuery and Bootstrap -->
<script src="js/jquery-3.2.1.min.js"></script>
<script src="js/bootstrap.bundle.min.js"></script>
<!-- Plugins JS -->
<script src="js/owl.carousel.min.js"></script>
<script src="js/slick.min.js"></script>
<!-- Custom JS -->
<script src="js/script.js"></script>
<script src="js/wow.min.js"></script>
<script>
    if($(window).width()>1199){
        new WOW().init();
    }
</script>

<script>
    /////////Search
    $(document).ready(function() {
        $(".fa-search").click(function() {
            $(".search-box").toggle();
            $("input[type='text']").focus();
        });

    });
</script>

<script>
    /* When the user clicks on the button,
    toggle between hiding and showing the dropdown content */
    function myFunction() {
        document.getElementById("myDropdown").classList.toggle("show");
    }

    // Close the dropdown if the user clicks outside of it
    window.onclick = function(event) {
        if (!event.target.matches('.dropbtn')) {
            var dropdowns = document.getElementsByClassName("dropdown-content");
            var i;
            for (i = 0; i < dropdowns.length; i++) {
                var openDropdown = dropdowns[i];
                if (openDropdown.classList.contains('show')) {
                    openDropdown.classList.remove('show');
                }
            }
        }
    }
</script>

<script>

    //Canvas-bg-footer


    let resizeReset = function() {
        w = canvasBody.width = window.innerWidth;
        h = canvasBody.height = window.innerHeight;
    }

    const opts = {
        particleColor: "#999",
        lineColor: "rgb(22,22,22)",
        particleAmount: 40,
        defaultSpeed: 1,
        variantSpeed: 1,
        defaultRadius: 4,
        variantRadius: 4,
        linkRadius: 350,
    };

    window.addEventListener("resize", function() {
        deBouncer();
    });

    let deBouncer = function() {
        clearTimeout(tid);
        tid = setTimeout(function() {
            resizeReset();
        }, delay);
    };

    let checkDistance = function(x1, y1, x2, y2) {
        return Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2));
    };

    let linkPoints = function(point1, hubs) {
        for (let i = 0; i < hubs.length; i++) {
            let distance = checkDistance(point1.x, point1.y, hubs[i].x, hubs[i].y);
            let opacity = 1 - distance / opts.linkRadius;
            if (opacity > 0) {
                drawArea.lineWidth = 0.5;
                drawArea.strokeStyle = `rgba(${rgb[0]}, ${rgb[1]}, ${rgb[2]}, ${opacity})`;
                drawArea.beginPath();
                drawArea.moveTo(point1.x, point1.y);
                drawArea.lineTo(hubs[i].x, hubs[i].y);
                drawArea.closePath();
                drawArea.stroke();
            }
        }
    }

    Particle = function(xPos, yPos) {
        this.x = Math.random() * w;
        this.y = Math.random() * h;
        this.speed = opts.defaultSpeed + Math.random() * opts.variantSpeed;
        this.directionAngle = Math.floor(Math.random() * 360);
        this.color = opts.particleColor;
        this.radius = opts.defaultRadius + Math.random() * opts.variantRadius;
        this.vector = {
            x: Math.cos(this.directionAngle) * this.speed,
            y: Math.sin(this.directionAngle) * this.speed
        };
        this.update = function() {
            this.border();
            this.x += this.vector.x;
            this.y += this.vector.y;
        };
        this.border = function() {
            if (this.x >= w || this.x <= 0) {
                this.vector.x *= -1;
            }
            if (this.y >= h || this.y <= 0) {
                this.vector.y *= -1;
            }
            if (this.x > w) this.x = w;
            if (this.y > h) this.y = h;
            if (this.x < 0) this.x = 0;
            if (this.y < 0) this.y = 0;
        };
        this.draw = function() {
            drawArea.beginPath();
            drawArea.arc(this.x, this.y, this.radius, 0, Math.PI * 2);
            drawArea.closePath();
            drawArea.fillStyle = this.color;
            drawArea.fill();
        };
    };

    function setup() {
        particles = [];
        resizeReset();
        for (let i = 0; i < opts.particleAmount; i++) {
            particles.push(new Particle());
        }
        window.requestAnimationFrame(loop);
    }

    function loop() {
        window.requestAnimationFrame(loop);
        drawArea.clearRect(0, 0, w, h);
        for (let i = 0; i < particles.length; i++) {
            particles[i].update();
            particles[i].draw();
        }
        for (let i = 0; i < particles.length; i++) {
            linkPoints(particles[i], particles);
        }
    }
    const canvasBody = document.getElementById("canvas"),
        drawArea = canvasBody.getContext("2d");
    let delay = 200,
        tid,
        rgb = opts.lineColor.match(/\d+/g);
    resizeReset();
    setup();

    //End-canvas-bg-footer

</script>

<script type="text/javascript">

    //Parallax

    (function () {
        var indexOf = [].indexOf;

        if (indexOf.call(window, 'ontouchstart') >= 0 === false) {
            $("body").mousemove(function (e) {
                var moveX, moveY;
                moveX = e.pageX * -1 / 25 + 'px';
                moveY = e.pageY * -1 / 25 + 'px';
                return $('.outer').css('background-position', 'calc(50% + ' + moveX + ') calc(50% + ' + moveY + ')');
            });
        }
    }).call(this);

</script>

</body>

</html>
