@extends('admin.app')
@section('content')
    <div class="content-right">
        <div class="topbar">
            <a class="menu-btn" href="#"><i class="fn menu-ic"></i></a>
        </div>
        <div class="">
        <div class="">
            <div class="">
                <a class="back btn" href="#"><i class="fn left-arrow"></i><span>Back to Homepage</span></a>
                <a class="logout btn" href="#"><i class="fn logout-ic"></i><span>Log Out</span></a>
                <div style="clear: both;"></div>
            </div>
            <div class="mrgn-60"></div>
            <div class="">
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">First</th>
                        <th scope="col">Last</th>
                        <th scope="col">Handle</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th scope="row">1</th>
                        <td>Mark</td>
                        <td>Otto</td>
                        <td>@mdo</td>
                    </tr>
                    <tr>
                        <th scope="row">2</th>
                        <td>Jacob</td>
                        <td>Thornton</td>
                        <td>@fat</td>
                    </tr>
                    <tr>
                        <th scope="row">3</th>
                        <td>Larry</td>
                        <td>the Bird</td>
                        <td>@twitter</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection