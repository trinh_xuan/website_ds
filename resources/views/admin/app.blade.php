<!doctype html>
<html lang="{{ app()->getLocale() }}">
<body>
<head>
    <link href="{{asset('css/admin/style.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
</head>
<div class="container sb-left">
    <div class="sidebar left">
        <div class="sidebar-logo">
            <img src="images/logo.png" width="128" alt="ds">
        </div>
        <div class="nav-list">
            <ul>
                <li><a href="{{route('admin')}}" class="active" data-page="statistaics"><i class="fn chart-pie"></i><span>HOME PAGE</span></a></li>
                <li><a href="" data-page=""><i class="fn users"></i><span>INTRODUCTION</span></a></li>
                <li><a href="#" data-page=""><i class="fn comment"></i><span>SERVICE</span></a></li>
                <li><a href="#" data-page=""><i class="fn megaphone"></i><span>PRODUTION</span></a></li>
                <li><a href="#" data-page=""><i class="fn chart-bar"></i><span>CATEGORIES</span></a></li>
                <li><a href="#" data-page=""><i class="fn location"></i><span>RECRUITMENT</span></a></li>
                <li><a href="#" data-page=""><i class="fn location"></i><span>CONTACT</span></a></li>
            </ul>
        </div>
    </div>
    @yield('content')
    <script type="text/javascript" src="{{asset('js/jquery-3.2.1.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/admin/admin.js')}}"></script>
</div>
</body>
</html>