<?php
/**
 * Created by PhpStorm.
 * User: macbook
 * Date: 6/24/19
 * Time: 3:39 PM
 */

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;

class HomePageController extends Controller
{
    public function __construct()
    {
        
    }
    public function index()
    {
        return view('admin.index');
    }

}